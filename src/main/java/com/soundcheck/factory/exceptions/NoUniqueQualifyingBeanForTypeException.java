package com.soundcheck.factory.exceptions;

public class NoUniqueQualifyingBeanForTypeException extends RuntimeException {
  public NoUniqueQualifyingBeanForTypeException(String message) {
    super(message);
  }
}
