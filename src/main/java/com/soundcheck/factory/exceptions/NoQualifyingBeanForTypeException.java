package com.soundcheck.factory.exceptions;

public class NoQualifyingBeanForTypeException extends RuntimeException {
  public NoQualifyingBeanForTypeException(String message) {
    super(message);
  }
}
