package com.soundcheck.factory.utils;

import com.ibm.icu.impl.Pair;
import com.soundcheck.factory.annotations.*;
import com.soundcheck.factory.exceptions.NoQualifyingBeanForTypeException;
import com.soundcheck.factory.exceptions.NoUniqueQualifyingBeanForTypeException;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FactoryUtils {

  private static Map<String, Object> objectMap = new ConcurrentHashMap<>(); // holds objects against class names

  private static Map<String, Object> factoryObjectMap = new ConcurrentHashMap<>(); // holds the factory class instance annotated with @FactoryConfig against class names

  private static Map<String, Method> beanToMethodMap = new ConcurrentHashMap<>(); // holds initializer methods annotated with @Bean against class names

  private static Map<String, Class<?>> beanToFactoryMap = new ConcurrentHashMap<>(); // holds the factory class reference annotated with @FactoryConfig against class names

  private static Reflections reflections;

  /**
   * @param consumer    The method that starts the application
   * @param params      The params for <i><b>consumer</b></i>
   * @param basePackage The package with the class annotated with <i><b>@FactoryConfig</b></i>
   * @apiNote This method provides a way for the application to start with <i><b>FactoryUtils</b></i> support
   */
  public static void init(String basePackage, Consumer<Object[]> consumer, Object... params) {
    List<String> basePackages = new ArrayList<>();
    Reflections tempReflections = new Reflections(basePackage);
    tempReflections.getTypesAnnotatedWith(FactoryConfig.class).stream().forEach(config -> {
      basePackages.addAll(Arrays.asList(config.getAnnotation(FactoryConfig.class).basePackages()));
      Stream.of(config.getDeclaredMethods()).filter(m -> m.isAnnotationPresent(Bean.class)).forEach(method -> {
        beanToFactoryMap.put(method.getReturnType().getName(), config);
        beanToMethodMap.put(method.getReturnType().getName(), method);
      });
    });
    reflections = new Reflections(new ConfigurationBuilder().forPackages(basePackages.stream().toArray(String[]::new))
        .setScanners(new SubTypesScanner()).setExecutorService(Executors.newFixedThreadPool(4)));
    consumer.accept(params);
  }

  /**
   * @param clazz Specified type
   * @return An object of the type <i><b>clazz</b></i>
   * @param T the class type for <i><b>clazz</b></i>
   * @throws NoQualifyingBeanForTypeException       If there is no eligible class
   * @throws NoUniqueQualifyingBeanForTypeException If there is more than one eligible class
   */
  public static <T> T getBean(Class<T> clazz) {
//    Map<String, Object> autowiredFieldNamesToObjects = Arrays.stream(clazz.getDeclaredFields())
//        .filter(field -> field.isAnnotationPresent(Autowired.class))
//        .collect(Collectors.toMap(field -> field.getName(), field -> getBean(field.getType())));

    T object = (T) computeIfAbsent(objectMap, clazz.getName(), className -> {
      return beanToMethodMap.containsKey(clazz.getName()) ? createObjectFromExistingBean(clazz)
          : createObject(clazz, new ArrayList<>(), new ArrayList<>());
    });

    Map<String, Object> autowiredFieldNamesToObjects = Arrays.stream(object.getClass().getDeclaredFields())
            .filter(field -> field.isAnnotationPresent(Autowired.class))
            .collect(Collectors.toMap(field -> field.getName(), field -> getBean(field.getType())));

    Arrays.stream(object.getClass().getDeclaredFields()).filter(
        field ->  autowiredFieldNamesToObjects.containsKey(field.getName())
    ).forEach(field -> {
      field.setAccessible(true);
      try {
        field.set(object, autowiredFieldNamesToObjects.get(field.getName()));
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    });
    return object;
  }

  /**
   * @param clazz  Specified type
   * @param args   List of types of the target constructor
   * @param values List of values of the target constructor
   * @param T the class type for <i><b>clazz</b></i>
   * @return An object of the type <i><b>clazz</b></i>
   * @throws NoQualifyingBeanForTypeException       If there is no eligible class with a constructor satisfying the list of <i><b>args</b></i>
   * @throws NoUniqueQualifyingBeanForTypeException If there is more than one eligible class with a constructor satisfying the list of <i><b>args</b></i>
   */
  public static <T> T getBean(Class<T> clazz, List<Class> args, List<Object> values) {
    Object[] valuesArray = values.stream().toArray(Object[]::new);
    return (T) computeIfAbsent(objectMap, clazz.getName(), className -> {
      if (beanToMethodMap.containsKey(clazz.getName())) return createObjectFromExistingBean(clazz, valuesArray);
      else return createObject(clazz, args, values);
    });
  }

  private static <T> T createObjectFromExistingBean(Class<T> clazz, Object... args) {
    try {
      return (T) beanToMethodMap.get(clazz.getName()).invoke(getFactory(beanToFactoryMap.get(clazz.getName())), args);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    } catch (InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }

  public static <T> T getFactory(Class<T> clazz) {
    return (T) computeIfAbsent(factoryObjectMap, clazz.getName(),
        className -> createObject(clazz, new ArrayList<>(), new ArrayList<>()));
  }

  private static <T> T createObject(Class<T> clazz, List<Class> args, List<Object> values) {
    Class<?>[] argsArray = args.stream().toArray(Class<?>[]::new);
    try {
      if (clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())) {
        clazz = (Class<T>) createObjectOfNonAbstractChildClass(clazz, argsArray);
      }
      return clazz.getConstructor(argsArray).newInstance(values.stream().toArray(Object[]::new));
    } catch (InstantiationException e) {
      throw new RuntimeException(e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    } catch (InvocationTargetException e) {
      throw new RuntimeException(e);
    } catch (NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  }

  private static <T> Class<? extends T> createObjectOfNonAbstractChildClass(Class<T> clazz, Class<?>[] argsArray) {
    List<Class<? extends T>> subTypes = reflections.getSubTypesOf(clazz).stream()
        .filter(type -> !Modifier.isAbstract(type.getModifiers()))
        .filter(type -> Stream.of(type.getConstructors()).anyMatch(c -> Arrays.equals(c.getParameterTypes(), argsArray)))
        .filter(type -> type.isAnnotationPresent(Component.class))
        .collect(Collectors.toList());

    if (subTypes.stream().anyMatch(type -> type.isAnnotationPresent(Primary.class))) {
      subTypes = subTypes.stream().filter(subType -> subType.isAnnotationPresent(Primary.class))
          .collect(Collectors.toList());
    }

    if (subTypes.size() < 1) {
      throw new NoQualifyingBeanForTypeException("No qualifying bean of type: " + clazz.getName());
    } else if (subTypes.size() > 1) {
      throw new NoUniqueQualifyingBeanForTypeException("No unique qualifying bean of type: " + clazz.getName());
    }

    return subTypes.get(0);
  }

  /**
   * @param map      A java.util.Map type variable on which the operation will be done.
   * @param key      The key of the <i><b>map</b></i>
   * @param computer A method reference which computes the value of a new <i><b>key</b></i>.
   * @param V        The value type of <i><b>map</b></i>
   * @param K        The key type of <i><b>map</b></i>
   * @return If the <i><b>key</b></i> already exists then the value is returned directly, otherwise the value computed with the <i><b>computer</b></i> is put into the map and then returned.
   */
  private static <V, K> V computeIfAbsent(Map<K, V> map, K key, Function<K, V> computer) {
    if (!map.containsKey(key)) {
      V object = computer.apply(key);
      map.put(key, object);
    }
    return map.get(key);
  }

}
