package com.soundcheck.app;

import com.soundcheck.factory.utils.FactoryUtils;

import java.util.function.Consumer;

public class SoundCheckApplication {

  public static void launch(String basePackage, Consumer<Object[]> player, String filePath, boolean doPlay) {
    FactoryUtils.init(basePackage, player, filePath, doPlay);
  }

}
