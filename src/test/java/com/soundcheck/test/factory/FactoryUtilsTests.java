package com.soundcheck.test.factory;

import com.soundcheck.factory.annotations.Autowired;
import com.soundcheck.factory.utils.FactoryUtils;
import com.soundcheck.processor.contracts.RaagBuilder;

import java.util.Arrays;

public class FactoryUtilsTests {

  public static void testBeanCreation() {
    FactoryUtils.init("com.soundcheck", args -> {
      RaagBuilder raagBuilder = FactoryUtils.getBean(RaagBuilder.class);
      boolean isInitialized = isProperlyInitialized(raagBuilder);
      if (isInitialized) System.out.println("RaagBuilder is initialized properly");
      else System.out.println("RaagBuilder is not initialized properly");
    }, new Object[]{new Object()});
  }

  private static boolean isProperlyInitialized(Object object) {
    if (object != null) {
      return Arrays.stream(object.getClass().getDeclaredFields())
          .filter(field -> field.isAnnotationPresent(Autowired.class))
          .noneMatch(field -> {
            field.setAccessible(true);
            try {
              return field.get(object) == null;
            } catch (IllegalAccessException e) {
              throw new RuntimeException(e);
            }
          });
    }
    return false;
  }

  public static void main(String[] args) {
    testBeanCreation();
  }

}
